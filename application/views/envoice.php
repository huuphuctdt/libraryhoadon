<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
</head>
<body>

<div id="container">
    <h1>Envoice</h1>
    <table class="table" border="1">
        <tr>
            <th>Create Invoice</th>
            <th>Cancel Invoice</th>
            <th>Update Invoice</th>
            <th>View Invoice</th>
            <th>Download Invoice</th>
        </tr>
        <tr>
            <td><a href="<?php echo "envoice/create"; ?>">Run</a></td>
            <td><a href="<?php echo "envoice/cancel"; ?>">Run</a></td>
            <td><a href="<?php echo "envoice/update"; ?>">Run</a></td>
            <td><a href="<?php echo "envoice/view"; ?>">Run</a></td>
            <td><a href="<?php echo "envoice/download"; ?>">Run</a></td>
        </tr>
    </table>
</div>

</body>
</html>