<?php

class Sample {

    public static function create(){
        $benBan = new BenBan();
        $benBan->setBenBanMaDonVi("123");
        $benBan->setBenBanTenDonVi("CÔNG TY CỔ PHẦN BỆNH VIỆN PHỤ SẢN QUỐC TẾ SÀI GÒN");
        $benBan->setBenBanMaSoThue("0301482886");
        $benBan->setBenBanDiaChi("63 Bùi Thị Xuân, P.Phạm Ngũ Lão, Quận 1, TP.HCM");

        $benMua = new BenMua();
        //$benMua->setBenMuaMaDonVi("");
        $benMua->setBenMuaMaSoThue("0101300842");
        $benMua->setBenMuaTenDonVi("Công ty phát triển công nghệ thái sơn");
        $benMua->setBenMuaHoTen("vu tho tuyền");
        $benMua->setBenMuaDiaChi("Số 99B tổ 70 Hồ Quỳnh, Phường Thanh Nhàn, Quận Hai Bà Trưng, Hà Nội");
        //$benMua->setBenMuaDienThoai("");
        //$benMua->setBenMuaFax("");
        //$benMua->setBenMuaEmail("");
        $benMua->setBenMuaTaiKhoanNganHang("0308808576");
        $benMua->setBenMuaTenNganHang("Ngân hàng Quân đội");

        $sanPham = new SanPham();
        $sanPham->setSoThuTu("1");
        $sanPham->setMaHang("");
        $sanPham->setTenHang("Máy tính Casio");
        $sanPham->setDonViTinh("cái");
        $sanPham->setSoLuong("1");
        $sanPham->setDonGia("500000");
        $sanPham->setThanhTien("500000");
        $sanPham->setVat("10");
        $sanPham->setTienVat("50000");

        $hinhThucThanhToan = new HinhThucThanhToan();
        $hinhThucThanhToan->setHinhThucThanhToan("01");
        $hinhThucThanhToan->setTamUng("0");
        $hinhThucThanhToan->setTienChietKhau("0");
        $hinhThucThanhToan->setTyGia("1");

        $hoaDon = HoaDon::XuatHoaDonDienTu(array($sanPham), $benBan, $benMua, $hinhThucThanhToan);
        return $hoaDon;
    }

    public static function cancel(){
        $hoaDon = new HoaDon();
        $hoaDon->setUserHuy("tuyenvt");
        $hoaDon->setSoHoaDon("0000017");
        $hoaDon->setNgayTaoHoaDon("2016-11-25");
        $hoaDon->setNoiDung("khách hàng không sử dụng dịch vụ");
        $result = HoaDon::HuyHoaDon($hoaDon);
        return $result;
    }

    public static function update(){
        $hoaDonGoc = new HoaDon();
        $hoaDonGoc->setMaLoaiHoaDon('01');
        $hoaDonGoc->setHoaDonId('22239');
        $hoaDonGoc->setSoHoaDon('0000048');
        $hoaDonGoc->setNgayNhapVien('0001-01-01');
        $hoaDonGoc->setNgayTaoHoaDon('2018-11-15');
        $hoaDonGoc->setNgayXuatHoaDon('2018-11-15');
        $hoaDonGoc->setMaEinvoice('883bc80c-661f-4f4b-82e8-90f0f5a2587d');
        $hoaDonGoc->setIsGiuLai('false');
        $hoaDonGoc->setIsSuDungBangKe('false');

        $benBan = new BenBan();
        $benBan->setBenBanMaDonVi("123");
        $benBan->setBenBanTenDonVi("CÔNG TY CỔ PHẦN BỆNH VIỆN PHỤ SẢN QUỐC TẾ SÀI GÒN");
        $benBan->setBenBanMaSoThue("0301482886");
        $benBan->setBenBanDiaChi("63 Bùi Thị Xuân, P.Phạm Ngũ Lão, Quận 1, TP.HCM");

        $benMua = new BenMua();
        $benMua->setBenMuaMaSoThue("0101300842");
        $benMua->setBenMuaTenDonVi("Công ty phát triển công nghệ thái sơn");
        $benMua->setBenMuaHoTen("vu tho tuyền");
        $benMua->setBenMuaDiaChi("Số 99B tổ 70 Hồ Quỳnh, Phường Thanh Nhàn, Quận Hai Bà Trưng, Hà Nội");
        $benMua->setBenMuaTaiKhoanNganHang("0308808576");
        $benMua->setBenMuaTenNganHang("Ngân hàng Quân đội");

        $sanPham = new SanPham();
        $sanPham->setSoThuTu("1");
        $sanPham->setMaHang("");
        $sanPham->setTenHang("Máy tính Casio");
        $sanPham->setDonViTinh("cái");
        $sanPham->setSoLuong("1");
        $sanPham->setDonGia("500000");
        $sanPham->setThanhTien("500000");
        $sanPham->setVat("10");
        $sanPham->setTienVat("50000");

        $hinhThucThanhToan = new HinhThucThanhToan();
        $hinhThucThanhToan->setHinhThucThanhToan("01");
        $hinhThucThanhToan->setTamUng("0");
        $hinhThucThanhToan->setTienChietKhau("0");
        $hinhThucThanhToan->setTyGia("1");

        $hoadonNew = HoaDon::UpdateHoaDon($hoaDonGoc, array($sanPham), $benBan, $benMua, $hinhThucThanhToan);

        return $hoadonNew;
    }

    public static function view(){
        $hoadonTemp = new HoaDon();
        $hoadonTemp->setMaEinvoice("c1722933-e903-46cd-b8c8-f7c71a5cb25a&iscd=0");
        $linkView = HoaDon::ViewHoaDon($hoadonTemp);
        return $linkView;
    }

    public static function download(){
        $hoadonTemp = new HoaDon();
        $hoadonTemp->setMaEinvoice("c1722933-e903-46cd-b8c8-f7c71a5cb25a&iscd=0");
        $linkView = HoaDon::ViewHoaDon($hoadonTemp);
        $file = HoaDon::DownloadHoaDon($linkView);
        return $file;
    }
    
}