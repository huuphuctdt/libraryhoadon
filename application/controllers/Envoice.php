<?php

class Envoice extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $arrayLibraries = [
            'einvoice/benban',
            'einvoice/benmua',
            'einvoice/sanpham',
            'einvoice/hinhthucthanhtoan',
            'einvoice/hoadon',
            'einvoice/helper',
            'einvoice/connect',
            'einvoice/sample'
        ];
        $this->load->library($arrayLibraries);
    }

    public function index(){
        $this->load->view('envoice');
    }

    public function create(){
        $result = Sample::create();
        return $result;
    }

    public function cancel(){
        $result = Sample::cancel();
        return $result;
    }

    public function update(){
        $result = Sample::update();
        return $result;
    }

    public function view(){
        $result = Sample::view();
        return $result;
    }

    public function download(){
        $result = Sample::download();
        return $result;
    }
}